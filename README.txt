Hello coder,

You have intercepted ten sets of confidential documents from the secret hacker group INSERT FLAVORFUL NAME HERE. Some of the documents are message logs, others are source code, and a few contain only some cryptic numbers or encrypted text.

The THEME for this CS Challenge is INSERT THEME HERE

Rules:

1. Teams of 2 people

2. Everything is allowed unless otherwise stated

3. Each problem folder contains two files. For example, folder A contains A.txt and D-hint.zip. A.txt is the problem statement and contains a problem for you to solve. After you solve problem A, you can use the answer to open the password-protected D-hint.zip, which contains a hint for problem D. If you have trouble opening the hint zips,  send Anthony a private message. Later problems may require hints from earlier problems.

4. Submit a form with your answers at the end of the contest. You can find the form here: INSERT FORM LINK HERE

5. Each problem is worth 10 points, except for J which worth 30 points. There may also be bonus questions after you solve J. Highest score wins.

You can view the latest rules and scoreboard using this link: INSERT SCOREBOARD LINK HERE

If you have any questions, send Anthony a private message.

Good luck and have fun.
